const user = {
    name: "Your Name",
    addresults: {
      personal: {
        line1: "101",
        line2: "street Line",
        city: "NY",
        state: "WX",
        coordinates: {
          x: 35.12,
          y: -21.49,
        },
      },
      office: {
        city: "City",
        state: "WX",
        area: {
          landmark: "landmark",
        },
        coordinates: {
          x: 35.12,
          y: -21.49,
        },
      },
    },
    contact: {
      phone: {
        home: "xxx",
        office: "yyy",
      },
      other: {
        fax: '234'
      },
      email: {
        home: "xxx",
        office: "yyy",
      },
    },
  };
  
//   let tempObj = new Object();
  
//   function objData(ans, user) {
//     if (typeof user !== "object") {
//         tempObj[ans] = user;
//         return;
//     } 
//     else {
//         for (let data in user) {
//             objData(ans + "_" + data, user[data]);
//         }
//     }
//   }
  
//   objData("user", user); 
//   console.log(tempObj);
  

function flattenObj(user, parent, result = {}){
    for(let key in user){
        let propName = parent ? parent + '_' + key : key;
        if(typeof user[key] == 'object'){
            flattenObj(user[key], propName, result);
        } else {
            result[propName] = user[key];
        }
    }
    return result;
}

var obj = flattenObj(user,"user")
console.log(obj)
  